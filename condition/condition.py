# encoding:utf-8

from datetime import datetime
import logging
import win32gui
import win32api
import win32con
import time

import pyautogui


config = {
    "level": logging.INFO,
    "format": "%(asctime)s : %(message)s",
    "datefmt": '%Y-%m-%d %H:%M:%S',
    "filename": 'myapp.log',
    "filemode": 'a'

}
logging.basicConfig(**config)

# condition_handle = None  # 空调按钮


# def call_back(handle, extra):
#     """
#     遍历句柄窗体程序结构
#     """
#     text = win32gui.GetWindowText(handle)
#     global condition_handle
#     if "2" in text:
#         condition_handle = handle


def control_gui(open_condition):
    """
    是否开关空调
    :param open_condition: True or False
    :return:None
    """
    handle = win32gui.FindWindow(None, "PLCUser")
    if handle <= 0:
        logging.info(u"无法找到空调控制器")
    else:
        win32gui.SetForegroundWindow(handle)
        plc_handle = win32gui.FindWindow("TfrmMain", None)
        #  win32gui.EnumChildWindows(plc_handle, call_back, None)

        rect = win32gui.GetWindowRect(plc_handle)
        location_x = rect[0]  # X坐标
        location_y = rect[1]  # Y坐标
        # size_with = rect[2] - location_x  # 窗体大小
        # size_height = rect[3] - location_y  # 窗体大小

        # 计算开关按键高度
        panel_position = (location_x, location_y)

        condition_panel_rel_locate = (150, 150)  # 相对坐标位置panel, x,y
        open_panel_rel_locate = (80, 215)  # 相对坐标位置panel，x,y
        close_panel_rel_locate = (160, 215)  # 相对坐标位置panel,x,y

        pyautogui.moveTo(*panel_position, duration=3)
        pyautogui.moveRel(*condition_panel_rel_locate, duration=3)
        pyautogui.click(button="left")
        # win32gui.PostMessage(condition_handle, win32con.WM_LBUTTONDBLCLK, win32con.MK_LBUTTON, 0)
        logging.info("find condition")

        if open_condition:
            pyautogui.moveTo(*panel_position, duration=3)
            pyautogui.moveRel(*open_panel_rel_locate, duration=3)
            pyautogui.click(button="left")
            logging.info("open air condition")

        else:
            pyautogui.moveTo(location_x, location_y, duration=3)
            pyautogui.moveRel(*close_panel_rel_locate, duration=3)
            pyautogui.click(button="left")
            logging.info("close air condition")


def run():
    now = datetime.now()
    hour = now.hour

    try:
        if 7 <= hour < 20:
            control_gui(True)
        else:
            control_gui(False)
    except Exception as e:
        logging.error(e)
    finally:
        time.sleep(2)
        img = pyautogui.screenshot()
        img.save("screen_shot.png")


if __name__ == "__main__":
    run()