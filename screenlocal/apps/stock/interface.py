# encoding:utf-8
import json
import re
from datetime import datetime, date
from math import ceil

import requests


def _compile_regex(content, regex):
    """

    :param content: str,
    :param regex: regex.compile,
    :return:
    """
    search_result = regex.search(content)
    if search_result:
        return search_result.groups()[0]


def market_real_time(code):
    """
    大盘数据接口
    上证：code:000001 set=zs
    深证：code:399001 set=zs
    中小板：code:399005 set=zs
    创业板: code:399006 set=zs

    get stock real time date
    :param code: stock code
    :return: dict or None
    """
    url = "http://q.stock.sohu.com/qp/hq?type=snapshot&code=%s&set=zs" % code
    response = requests.request("GET", url)
    if response.ok:
        content = response.content.decode("gbk")
        try:
            result = eval(content)
        except SyntaxError:
            return None

        return {
            "datetime": datetime.strptime(result[0], "%Y-%m-%d %H:%M:%S"),  # 时间
            "name": unicode(result[2], "utf8"),  # 名称
            "change": float(result[4]),  # 上涨价格
            "increase": float(result[5].replace("%", "")),  # 涨幅，百分比
            "current": float(result[3]),  # 当前价格
            "open": float(result[12]),  # 开盘价格
            "high": float(result[14]),  # 最高价格
            "low": float(result[16]),  # 最低价格
            "last_close": float(result[10]),  # 昨日收盘价格
            "total_sum": int(result[21]),  # 成交额 多少万元，
            "volume": int(result[17]),  # 成交量
            "swing": result[-2],  # 振幅
        }


def _stock_content(content):
    "处理腾讯股票数据接口信息"
    regex = re.compile(r'"(.*)"')
    result = regex.findall(content)
    if result:
        return result[0].split('~')


def stock_date(market, code):
    """
    获取股票当日数据
    腾讯API, API地址：http://qt.gtimg.cn/q=sh600383
    sh:上海
    sz:深圳
    返回当天成交数据
    code:股票代码
    market：股票市场
    @return dict
    """

    if code and market:
        url = 'http://qt.gtimg.cn/q=%s%s' % (market, code)
        response = requests.request("GET", url)
        if response.ok:
            content = response.content.decode(response.encoding).encode("utf8")
            stock = _stock_content(content)
            if stock:
                return {
                    "code": code,  # 股票代码
                    "name": unicode(stock[1], 'utf8'),  # 股票名称
                    "last_close": float(stock[4]),  # 昨日收盘价格
                    "open": float(stock[5]),  # 开盘价格
                    "current": float(stock[3]),  # 当前收盘价格（可以是当前价格）
                    "high": float(stock[33]),  # 最高价格
                    "low": float(stock[34]),  # 最低价格
                    "change": float(stock[31]),  # 涨跌价格
                    "increase": float(stock[32]),  # 涨跌比%
                    "volume": int(stock[6]),  # 成交量（手）
                    "total_sum": int(stock[37]),  # 成交额（万元）
                    "datetime": datetime.strptime(stock[30], "%Y%m%d%H%M%S")  # stock[30][:8]  # 时间
                }


def current_day(market, code):
    """
    get current day stock line
    url : http://d.10jqka.com.cn/v2/time/sh_600383/last.js
    :param market: 'sh' or 'sz'
    :param code: stock code
    :return: yield dict, (time,stock price, trading amounts, avg price, volume)
    """
    assert market in ("sh", "sz")

    url = "http://d.10jqka.com.cn/v2/time/{market_code}_{stock_code}/last.js".format(market_code=market,
                                                                                     stock_code=code)
    response = requests.request("GET", url)
    if response.ok:
        regex = re.compile(r'\((.*)\)')
        content = _compile_regex(response.content, regex)
        if content:
            stock_data = json.loads(content, encoding="utf-8")

            price_series = stock_data["%(market)s_%(code)s" % {"market": market, "code": code}]["data"]
            price_series = price_series.split(";")

            current = date.today().strftime("%Y-%m-%d")
            date_format = "%Y-%m-%d %H%M"
            for time_series in price_series:
                items = time_series.split(",")
                # item[0] datetime, item[1] price ,item[2] total money, item[3] avg ,item[4] volume,
                yield {
                    "time": datetime.strptime(current + " " + items[0], date_format),
                    "price": float(items[1]),
                    "total": float(items[2]),
                    "avg": float(items[3]) if items[3] else 0,
                    "volume": int(items[4]) if items[4] else 0
                }


def stock_kline(market, code, year):
    """
    get stock candlesticks data

    url: http://qd.10jqka.com.cn/api.php?p=stock_day&info=k_sz_000005&year=2012,2013&fq=

    :param market: sh, sz
    :param code: stock code
    :return: yield dict,date 日期, open 开盘价格, high 最高价格, low 最低价格, close 收盘价格, volume 成交量, total 成交额
    """
    assert market in ("sh", "sz")
    url = "http://qd.10jqka.com.cn/api.php?p=stock_day&info=k_%(market)s_%(code)s&year=%(years)s&fq=" % \
          {"market": market, "code": code, "years": year}
    response = requests.request("GET", url)
    if response.ok:
        regex = re.compile(r"=(.*)\|$")
        content = _compile_regex(response.content, regex)
        if content:
            day_series = content.split("|")
            for day_item in day_series:
                day_kline = day_item.split(",")
                yield {
                    "date": datetime.strptime(day_kline[0], "%Y%m%d").date(),
                    "open": float(day_kline[1]),
                    "high": float(day_kline[2]),
                    "low": float(day_kline[3]),
                    "close": float(day_kline[4]),
                    "volume": int(ceil(float(day_kline[5]))),
                    "total": int(ceil(float(day_kline[6])))
                }


def get_current_stock(code, market_type):
    """
    :param code:
    :return:
    """
    assert market_type in ["us", "hk", "sh", "sz"]
    url = "https://gupiao.baidu.com/api/rails/stockbasicbatch"

    code = market_type + code
    params = {
        "format": "json",
        "stock_code": code
    }

    response = requests.request("get", url, params=params)
    if response.status_code == 200:

        content = eval(response.content)
        if content["errorNo"] == 0:
            result = content["data"][0]

            result["stockName"] = result["stockName"].decode('unicode-escape')

            return result
