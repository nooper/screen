# encoding:utf-8
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.http.response import HttpResponse, HttpResponseRedirect

from django.core import serializers
from django.views.generic import TemplateView
import json
from django.http.response import JsonResponse
from django.contrib.auth.decorators import login_required
from django.views.decorators.csrf import csrf_protect
from django.template import RequestContext
from django.contrib.auth import authenticate, login
from django.contrib import messages

from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.views import logout_then_login
from django.shortcuts import redirect


class LogoutView(TemplateView):
    """

    """
    template_name = "web/accounts/logout.html"

    def get(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        return logout_then_login(request)


class LoginView(TemplateView):
    template_name = "web/accounts/login.html"

    @method_decorator(csrf_protect)
    def post(self, request, *args, **kwargs):
        """

        :param request:
        :param args:
        :param kwargs:
        :return:
        """
        username = request.POST.get("username", "")
        password = request.POST.get("password", "")
        remember = request.POST.get("remember", False)
        if username and password:
            user = authenticate(username=username, password=password)
            if user and user.is_active:
                if not remember:
                    request.session.set_expiry(0)
                login(request, user)
                return redirect("index")
            else:
                messages.add_message(request, messages.INFO, u"用户名或者密码错误")
        else:
            messages.add_message(request, messages.INFO, u"请输入用户名或密码")
        return render(request, self.template_name, context_instance=RequestContext(request))


class IndexView(TemplateView):
    """
    """
    template_name = "web/index.html"
