from django.conf.urls import patterns, url
from django.contrib.auth.decorators import login_required
from . import views


urlpatterns = patterns('',
                       url(r"^login$", views.LoginView.as_view(), name="login"),
                       url(r'^logout$', views.LogoutView.as_view(), name="logout"),
                       url(r'^web$',login_required(views.IndexView.as_view()),name="index"),
                       )
