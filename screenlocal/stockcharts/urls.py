# encoding:utf-8

from django.conf.urls import patterns, url

from stockcharts import views


urlpatterns = patterns('',
                       url(r'^$', views.Market.as_view(), name="stock"),
                       url(r"^candles$", views.candles, name="candles"),
                       url(r"^real_time$", views.real_data, name="real_time"),
                       url(r'^line$', views.chart_line, name="line"),
                       url(r'^scroll$', views.ScrollBar.as_view(), name="stock_scroll"),
                       url(r'^scroll2$',views.ScrollBarNew.as_view(),name=""),
                       url(r'^codes$',views.get_codes,name="codes"),
                       url(r'^stock_data',views.stock_data,name="stock_data"),
                       )