# encoding:utf-8
from datetime import datetime, date
import time

from stockcharts.models import StockLine
from apps.stock import interface


def sleep(second):
    """
    decorate for func sleep time
    装饰器：用于一定的周期频率获取数据，通过
    @sleep(1) 等待一秒钟的周期获取数据信息。
    """

    def _spend(func):
        today = date.today()
        start_time1 = today.isoformat() + " 9:30:00"
        start_time2 = today.isoformat() + " 13:00:00"
        end_time1 = today.isoformat() + " 11:30:00"
        end_time2 = today.isoformat() + " 15:00:00"
        datetime_format = "%Y-%m-%d %H:%M:%S"

        def _time(*args, **kwargs):
            while True:
                start1 = datetime.strptime(start_time1, datetime_format)
                start2 = datetime.strptime(start_time2, datetime_format)
                end1 = datetime.strptime(end_time1, datetime_format)
                end2 = datetime.strptime(end_time2, datetime_format)
                now = datetime.now()
                if (now >= start1 and now <= end1) or (now >= start2 and now <= end2):
                    func(*args, **kwargs)
                else:
                    time.sleep(second)

        return _time

    return _spend


def parser_day_series(code,market):
    stock_items = [item for item in interface.current_day(market, code)]
    stock_items.sort(key=lambda x: x["time"], reverse=True)

    for item in stock_items:
        if not StockLine.objects.filter(market=market, code=code, current_date=item["time"]).exists():
            stock_line = StockLine(market=market,
                                   code=code,
                                   price=item["price"],
                                   current_date=item["time"],
                                   avg=item["avg"],
                                   volume=item["volume"],
                                   total=item["total"])
            stock_line.save()
        else:
            break


