# encoding:utf-8
from django.contrib import admin

from .models import StockHistory, StockLine,StockCode


class StockHistoryAdmin(admin.ModelAdmin):
   pass

class StockLineAdmin(admin.ModelAdmin):
    pass

class StockCodeAdmin(admin.ModelAdmin):
    list_display = ("global_market","code")


admin.site.register(StockCode,StockCodeAdmin)
admin.site.register(StockLine, StockLineAdmin)
admin.site.register(StockHistory, StockHistoryAdmin)