# encoding:utf-8

from datetime import date

from django.db.models import Q
from django.http import JsonResponse
from django.views.decorators.cache import cache_page
from django.views.generic import TemplateView

from apps.stock import interface
from stockcharts.models import StockHistory, StockLine
from .models import StockCode
from .tasks import parser_day_series


class Market(TemplateView):
    """
    指数数据发布
    """
    template_name = "markets.html"

    def get(self, request, *args, **kwargs):
        """
        """
        code = request.GET.get("code", "399001")  # 默认深交所
        market = request.GET.get("market", "sz")
        if code and market:
            stock_history = [stock_item for stock_item in interface.stock_kline(market, code, date.today().year)]
            stock_history.sort(key=lambda x: x["date"], reverse=True)
            for item in stock_history:
                if not StockHistory.objects.filter(date=item["date"], market=market, code=code).exists():
                    stock_day = StockHistory(market=market, code=code, open=item["open"], low=item["low"],
                                             close=item["close"], high=item["high"],
                                             volume=item["volume"], total=item["total"], date=item["date"])
                    stock_day.save()
                else:
                    break
        return super(Market, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        """
        """
        context = super(Market, self).get_context_data(**kwargs)
        context["code"] = self.request.GET.get("code", "399001")
        return context


class ScrollBar(TemplateView):
    """
    滚动条
    """
    template_name = "scroll.html"

    def get_context_data(self, **kwargs):
        context = super(ScrollBar, self).get_context_data(**kwargs)
        context["code"] = self.request.GET.get("code", "399001")
        context["market"] = self.request.GET.get("market", "sz")

        return context


class ScrollBarNew(TemplateView):
    """
    """
    template_name = "scroll2.html"


cache_page(60 * 15)


def get_codes(request):
    """
    :param request:
    :return:
    """
    market = request.GET.get("market", "")

    if market == "hk":
        all_stock = StockCode.objects.filter(global_market="hk").all()
    elif market == "us":
        all_stock = StockCode.objects.filter(global_market="us").all()
    else:
        all_stock = StockCode.objects.filter(Q(global_market="sh") | Q(global_market="sz")).all()
    stock_list = []
    for item in all_stock:
        stock_list.append({
            "code": item.code,
            "market": item.global_market
        })

    context = {
        "stocks": stock_list
    }

    return JsonResponse(context)


def chart_line(request):
    """

    :param request:
    :return:
    """

    code = request.GET.get("code", "")
    market = request.GET.get("market", "")

    parser_day_series(code, market)

    now = date.today()
    query_set = StockLine.objects.filter(code=code, market=market,
                                         current_date__year=now.year,
                                         current_date__month=now.month,
                                         current_date__day=now.day).order_by("current_date")

    filter_dict = {"key": code,
                   "values": []}
    for item in query_set:
        filter_dict["values"].append({"price": float(item.price),
                                      "volume": item.volume / 10000.0,
                                      "total": item.total / 10000.0,
                                      "date": item.current_date
                                      }
                                     )

    return JsonResponse(filter_dict)


def real_data(request):
    """
    get the real time data
    :param request:
    :return:
    """

    code = request.GET.get("code", "")
    market = request.GET.get("market", "")

    if code in ("000001", "399001", "399005", "399006"):

        stock_data = interface.market_real_time(code)
    else:
        stock_data = interface.stock_date(market, code)

    return JsonResponse(stock_data)


@cache_page(300)
def stock_data(request):
    """
    :param request:
    :return:
    """
    code = request.GET.get("code", "")
    market = request.GET.get("market", "")

    if market == "sh":
        response = interface.get_current_stock(code, "sh")
    elif market == "sz":
        response = interface.get_current_stock(code, "sz")
    elif market == "us":
        response = interface.get_current_stock(code, "us")
    elif market == "hk":
        response = interface.get_current_stock(code, "hk")

    return JsonResponse(response)


def candles(request):
    """
    :param request:
    :return:
    """
    code = request.GET.get("code", "")
    market = request.GET.get("market", "")
    stock_history = StockHistory.history(market, code)

    history_dict = {
        "key": code,
        "values": []}
    for item in stock_history:
        history_dict["values"].append({
            "high": item.high,
            "open": item.open,
            "low": item.low,
            "close": item.close,
            "volume": item.volume,
            "date": item.date
        })
    return JsonResponse(history_dict)
