# encoding:utf-8

import datetime

from django.db import models


class StockCode(models.Model):
    """
    """
    global_choices = (("sh","Shanghai"),("sz","Shenzhen"),("us","US"),("hk","hk"))
    global_market = models.CharField(choices=global_choices,max_length=10,)
    code = models.CharField(max_length=20,unique=True)




class StockBase(models.Model):
    market = models.CharField(max_length=5, choices=(("sh", u"上海"), ("sz", u"深圳")), verbose_name=u"市场")
    code = models.CharField(max_length=10, verbose_name=u"股票代码")
    volume = models.BigIntegerField()
    total = models.BigIntegerField()

    def __unicode__(self):
        return u"code:%s market:%s volume:%s total:%s" % (self.code,self.market,self.volume,self.total)

    class Meta:
        abstract = True


class StockHistory(StockBase):
    """
    """
    high = models.DecimalField(max_digits=9, decimal_places=2, verbose_name=u"最高价")
    open = models.DecimalField(max_digits=9, decimal_places=2, verbose_name=u"开盘价")
    low = models.DecimalField(max_digits=9, decimal_places=2, verbose_name=u"最低价")
    close = models.DecimalField(max_digits=9, decimal_places=2, verbose_name=u"收盘价")
    date = models.DateField(verbose_name=u"日期")

    @classmethod
    def history(cls, market, code, period=45):
        """
        """
        now = datetime.datetime.now()
        return cls.objects.filter(market=market, code=code, date__lte=now.date()).order_by("date")[:period]

    def __unicode__(self):
        data = super(StockHistory, self).__unicode__()
        return u"SockHistory(%s high:%s open:%s low:%s close:%s date:%s)" % (
            data, self.high, self.open, self.low, self.close, self.date)


class StockLine(StockBase):
    """
    """
    price = models.DecimalField(max_digits=9, decimal_places=2)
    current_date = models.DateTimeField()
    avg = models.DecimalField(max_digits=9, decimal_places=2, verbose_name=u"平均值")

    class Meta:
        unique_together = ("current_date", "code")

    def __unicode__(self):
        data = super(StockLine, self).__unicode__()
        return u"StockLine( %s price:%s avg:%s current_date:%s)" % (data, self.price, self.avg, self.current_date)


