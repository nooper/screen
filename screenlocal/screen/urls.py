# encoding:utf-8

from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns, static

urlpatterns = patterns('',

                       url(r'^admin/', include(admin.site.urls)),
                       # url(r"^",include('apps.web.urls')),
                       #url(r'^accounts/login/$', 'django.contrib.auth.views.login'),
                       # url(r'^chat/', include('live_chat.urls')),
                       url(r'^market/', include('stockcharts.urls')),
                       # url(r'^image/', include('image_show.urls')),
                       # url(r"^mobile/", include('image_show.mobile_urls')),
                       )

if settings.DEBUG:
    urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)