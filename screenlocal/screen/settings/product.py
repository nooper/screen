# encoding:utf-8

from .core import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': "screen",
        "PORT": "3306",
        "HOST": "127.0.0.1",
        "USER": "root",
        "PASSWORD": "root"
    }
}