# encoding:utf-8

from .base import *

THUMBNAIL_ALIASES = {
    '': {
        'screen': {"size":(704, 400), "quality":85, "crop":"smart"},
    },
}
