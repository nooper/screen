# encoding:utf-8

from django.db import models


class Post(models.Model):

    message = models.TextField(verbose_name=u"内容")
    post_date = models.DateTimeField(auto_now_add=True,verbose_name=u"发布日期")

    def __unicode__(self):
        return "message:%s date:%s" % (self.message[:15],self.post_date)
