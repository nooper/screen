# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.TextField(verbose_name='\u5185\u5bb9')),
                ('post_date', models.DateTimeField(auto_now_add=True, verbose_name='\u53d1\u5e03\u65e5\u671f')),
            ],
        ),
    ]
