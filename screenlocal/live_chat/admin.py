# encoding:utf-8
from django.contrib import admin
from .models import Post


class PostAdmin(admin.ModelAdmin):
    list_display = ("message", "post_date")

admin.site.register(Post, PostAdmin)
