# encoding:utf-8

import json

from django.views.generic import TemplateView

from django.http import HttpResponse, JsonResponse

from .models import Post

class Chart(TemplateView):
    """
    """
    template_name = "chat/chat.html"

def message_jsonp(request):
    """
    javascript jsonp callback,
    """
    call_back = request.GET.get("callback", "")
    post_json = ""
    if Post.objects.exists():
        post = Post.objects.order_by("-post_date").first()
        post_json = json.dumps({
            "message": post.message,
            "count": Post.objects.count(),
        })
    response = call_back + "(" + post_json + ")"
    return HttpResponse(response, content_type="application/json")


def message(request):
    """
    """
    response_data = {}
    if Post.objects.exists():
        post = Post.objects.order_by("-post_date").first()
        response_data = {
            "message": post.message,
            "count": Post.objects.count(),
            "date": post.post_date
        }
    return JsonResponse(response_data)