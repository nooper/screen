# encoding:utf-8

from django.conf.urls import patterns, url
from . import views

urlpatterns = patterns('',
                       url(r'^post$', views.message, name="post_message"),
                       url(r"^$", views.Chart.as_view(), name="live_chat"),
                       url(r'^post_jsonp$', views.message_jsonp, name="post_jsonp_message")
                       )